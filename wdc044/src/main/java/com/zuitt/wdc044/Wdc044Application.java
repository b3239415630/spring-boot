package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// "@SpringBootApplication" is an example of "Annotations" mark.
// Annotations are used to provide supplemental information about the program.
// These are used to manage and configure the behavior of the framework.
// Annotations are used extensively in Spring Boot to configure components, define dependencies, or enable specific functionalities
// Spring Boot scans for classes in its class path upon running and assigns behaviors on them based on their annotations.

//This specifies the main class of the spring boot application
@SpringBootApplication

// This indicate that a class is a controller that will handle RESTful request and returns an HTTP response.
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		// This method starts the whole Spring Framework.
		// This is also serves as the entry point to start of the application.
		SpringApplication.run(Wdc044Application.class, args);
	}

	// This is used the map HTTP GET request
		// Note: HTTP Request annotations is usually followed by a "method body"
		// "method body" contains the logic to generate the response.
	@GetMapping("/hello")

	// "@RequestParam" is used to extreact query parameters, form parameters, and even files from the request
		// if the URL is: /hello?name=john the method will return "Hello john"
			// "?" means taht the start of the parameters followed by "key=balue" pair.
		// if the URL is: /hello the method will return "Hello World"
		// TO send a response of "Hello + name"
	public  String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s!", name);
	}

	//ACTIVITY 01

	@GetMapping("/hi")

	public String hi(@RequestParam(value = "user", defaultValue  = "user!") String user){
		return String.format("hi %s!", user);
	}

	@GetMapping("/nameAge")

	public  String nameAge(@RequestParam(value = "name", defaultValue = "John")String name, @RequestParam(value = "age", defaultValue = "28") int age) {
		return String.format("hi %s!Your age is %d.", name, age);
	}

}
